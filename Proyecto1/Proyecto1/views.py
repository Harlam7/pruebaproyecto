from django.http import HttpResponse
import datetime
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render

#-----------------------------------------------------------------------#

#Creamos la primera vista de forma:
#def  saludo(request): #primera vista
#	return HttpResponse("buenas buenas conshesumare")

#def despedida(request):
#	return HttpResponse("todo bien chunchirria")
#	

#-----------------------------------------------------------------------#
class Persona(object):

	def __init__(self, nombre, apellido):

		self.nombre=nombre
		self.apellido=apellido
		

#-----------------------------------------------------------------------#

#Creamos otra vista:

def saludo(request):

	p1=Persona("Nicolás", "Duque")

	#nombre="Harlam"
	#apellido="7"
	
	temasCurso=["Plantillas", "Modelos", "Formularios", "Vistas", "Despliegue"]

	fecha_actual=datetime.datetime.now()

	# UNO: si solo queremos mostrar una plantilla en especifico 
	#doc_externo=get_template('miplantilla.html')


	#documento="""<html>
	#<body>
	#<h1>
	#ola amigos de iutub
	#</h1>
	#</body>
	#</html>"""
	

	#return HttpResponse("hola, wenas a todos")
	#doc_externo=open("/home/harlam/proyecto3/Proyecto1/Proyecto1/plantillas/miplantilla.html")

	#Creamos el objeto tipo Template
	#plt = plantilla XD
	##en los arghumentos del contructosr, que lea este docuemnto que tenemos almacenados en doc_externo
	#plt=Template(doc_externo.read())

	#cerramos la "comunicación"
	#doc_externo.close()

	#Creamos el contexto(argumento), datos adicionales para el template(variables, funciones, etc)
	#ctx = contexto 
	#ctx=Context({"nombre_persona":p1.nombre, "apellido_persona":p1.apellido, "actual":fecha_actual, "temas":temasCurso})

	#Renderizamos el documento que se encuentra guardado en plt
	#documento=plt.render(ctx)
	# UNO: si solo queremos mostrar una plantilla en especifico 
	#documento=doc_externo.render({"nombre_persona":p1.nombre, "apellido_persona":p1.apellido, "actual":fecha_actual, "temas":temasCurso})

	#return HttpResponse(documento)
	
	return render(request, "miplantilla.html", {"nombre_persona":p1.nombre, "apellido_persona":p1.apellido, "actual":fecha_actual, "temas":temasCurso})

#-----------------------------------------------------------------------#
# PLANTILLA 1
def plantilla1(request):

	fecha_actual=datetime.datetime.now()

	return render(request, "plantilla1.html", {"dameFecha":fecha_actual})

#-----------------------------------------------------------------------#
#PLANTILLA 2
def plantilla2(request):

	fecha_actual=datetime.datetime.now()

	return render(request, "plantilla2.html", {"dameFecha":fecha_actual})
	
#-----------------------------------------------------------------------#

def despedida(request):

	return HttpResponse("bai")

#-----------------------------------------------------------------------#

def dameFecha(request):

	fecha_actual=datetime.datetime.now()

	documento="""<html>
	<body>
	<h1>
	Fecha y hora actuales %s
	</h1>
	</body>
	</html>"""%fecha_actual

	return HttpResponse(documento)

#-----------------------------------------------------------------------#

def calculaEdad(request, edad, agno):

	#edadActual=25
	periodo=agno-2020
	edadFutura=edad+periodo
	documento="<html><body><h1>En el año %s vas a tener %s"%(agno, edadFutura)

	return	HttpResponse(documento)

#def plantilla1(request):	
#	return render(request, "plantilla1.html")
